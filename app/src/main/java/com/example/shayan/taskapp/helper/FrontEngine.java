package com.example.shayan.taskapp.helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;


import com.example.shayan.taskapp.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class FrontEngine {


    public String accessToken = "";
    //    public String accessToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE1LCJpc3MiOiJodHRwOlwvXC9kZXYtY21vbGRzLmNvbVwvYXBwdW5pdHdvcmtcL25hdmlnYXRpb25hcHBcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTUxMDkxNzkyOCwiZXhwIjoxNTEyMTI3NTI4LCJuYmYiOjE1MTA5MTc5MjgsImp0aSI6IjkxNmRlMWIxNzFjNzA1ZDRlNzY5MGJmZmRhMGRkODkyIn0.xWaqiMTH_ztN-EENyZN3iVi84l8Akhbxf9FMw_ejfyg";
    private String userFileName = "user_detail.ser";
   // public UserData userData = null;
    private File userDetailFile = null;

    public UserModel profile=null;



    public boolean isPlaylist = false;
    public static Uri imagePathUri=null;
    public static String[] permissionsWriteStorage = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};


    public static String pictureImagePath=null;

    public boolean isNotificationCome = false;

    public String AudioSavePathInDevice = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "AudioRecording.3gp";
    ;

    public String AudioEditPathInDevice = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "EditedRecording.3gp";
    ;

    public boolean rememberUser = true;

    public boolean isEdit = false;

    public boolean isPlayInBackground = true;

    public static FrontEngine frontEngine = null;

    public static FrontEngine getInstance() {
        if (frontEngine == null)
            frontEngine = new FrontEngine();
        return frontEngine;
    }

    public String MainApiUrl = "https://dev-cmolds.com/ulearn2/public/api/";

    public String weather = "";


    public String secsToTime(int totalSecs) {


        int hours = totalSecs / 3600;
        int minutes = (totalSecs % 3600) / 60;
//        int seconds = totalSecs % 60;

        String timeString = "";//String.format("%02d hr %02d min", hours, minutes);
        if (hours > 0) {
            timeString = String.format("%02d hr %02d min", hours, minutes);
        } else {
            timeString = String.format("%02d min", minutes);
        }

        return timeString;
    }

    public String secsToTimeFormat(long totalSecs) {

        SimpleDateFormat sdf = new SimpleDateFormat("h:mm a");
        Date resultdate = new Date(totalSecs);
        return (sdf.format(resultdate));
    }

    public String secsToDateFormat(long totalSecs) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM");
        Date resultdate = new Date(totalSecs);
        return (sdf.format(resultdate));
    }


    public UserModel getProfile() {
        return profile;
    }

    public void setProfile(UserModel profile) {
        this.profile = profile;
    }

    public String secsToDayFormat(long totalSecs) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd");
        Date resultdate = new Date(totalSecs);
        return (sdf.format(resultdate));
    }

    public String secsToMonthFormat(long totalSecs) {

        SimpleDateFormat sdf = new SimpleDateFormat("MMM");
        Date resultdate = new Date(totalSecs);
        return (sdf.format(resultdate));
    }

    public String secsToFullDateFormat(long totalSecs) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        Date resultdate = new Date(totalSecs);
        return (sdf.format(resultdate));
    }

    public String secsToFullDate2Format(long totalSecs) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd - MMM - yyyy");
        Date resultdate = new Date(totalSecs);
        return (sdf.format(resultdate));
    }

    public String getMiles(Double i) {

        return String.format("%.1f mi", i * 0.000621371192);
    }

  /*  private RetrofitFactory retrofitFactory;

    public RetrofitFactory getRetrofitFactory() {
        retrofitFactory = new RetrofitFactory();
        return retrofitFactory;
    }
*/
  public void changeLang(String lang, Context context) {
      Locale myLocale;
      if (lang.equalsIgnoreCase(""))
          return;
      myLocale = new Locale(lang);
      Locale.setDefault(myLocale);
      android.content.res.Configuration config = new android.content.res.Configuration();
      config.locale = myLocale;
      context.getResources().updateConfiguration(config,
              context.getResources().getDisplayMetrics());
  }
   private ArrayList<UserModel> arrListCategories;

    public ArrayList<UserModel> getArrListCategories() {
        return arrListCategories;
    }

    public void setArrListCategories(ArrayList<UserModel> arrListCategories) {
        this.arrListCategories = new ArrayList<>();
        this.arrListCategories = arrListCategories;
    }

    public HashMap getMap(String[] strings) {
        HashMap<String, String> map = new HashMap<>();
        if (strings != null && strings.length > 1) {
            for (int i = 0; i < strings.length; i += 2)
                map.put(strings[i], strings[i + 1]);
        }
        return map;
    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    private File categoryFile = null;
    private String categoryFileName = "category.ser";

   /* private ArrayList<CategoryModel> arrListCategory;
*/
    /*public void setArrListCategory(Context context, ArrayList<CategoryModel> arrListCategory) {
        categoryFile = new File(context.getFilesDir(), categoryFileName);
        try {
            SerializationUtil.serialize(new Gson().toJson(arrListCategory), categoryFile);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
*/
  /*  public ArrayList<CategoryModel> getArrListCategory(Context context) {
        arrListCategory = new ArrayList<>();
        JSONArray jr = null;
        try {
            categoryFile = new File(context.getFilesDir(), categoryFileName);
            String userString = (String) SerializationUtil.deserialize(categoryFile);

            try {
                jr = new JSONArray(userString);
                if (jr != null) {
                    for (int i = 0; i < jr.length(); i++) {
                        CategoryModel categoryModel;
                        categoryModel = new Gson().fromJson(String.valueOf(jr.get(i)), CategoryModel.class);
                        arrListCategory.add(categoryModel);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ;
*/
/*

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return arrListCategory;
    }

    public UserData getUserData() {
        return userData;
    }
*/

  /*  public UserData initializeUser(Context context) {


        try {
            userDetailFile = new File(context.getFilesDir(), userFileName);

            if (userDetailFile != null) {
                String userString = (String) SerializationUtil.deserialize(userDetailFile);
                this.userData = new Gson().fromJson(userString, UserData.class);
                if (userData.getAccess_token() != null) {
                    this.accessToken = "Bearer " + userData.getAccess_token();
                    SharedPreference.getInstance(context).saveValueInSharedPreference(context.getString(R.string.sp_accesstoken), accessToken);
                }
            } else {
                this.userData = new UserData();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (userData == null) {

            userData = new UserData();
        }

        return userData;
    }

    public boolean saveUser(Context context, UserData user) {

        File userDetailFile = new File(context.getFilesDir(), userFileName);
        if (user != null) {
            try {

//                user.setAccess_token("Bearer " + user.getAccess_token());
                SerializationUtil.serialize(new Gson().toJson(user), userDetailFile);
                this.userData = user;
                if (user.getAccess_token() != null) {
                    this.accessToken = "Bearer " + user.getAccess_token();
                    SharedPreference.getInstance(context).saveValueInSharedPreference(context.getString(R.string.sp_accesstoken), accessToken);
                }

            } catch (IOException e) {
                //e.printStackTrace();
                return false;
            } catch (ClassNotFoundException e) {
                //e.printStackTrace();
                return false;
            }

            return true;
        } else {
            return false;
        }

    }
*//*
    public boolean deleteUserFile(Context context) {
        boolean deleted = false;
        userDetailFile = new File(context.getFilesDir(), userFileName);

        if (userDetailFile != null) {
            deleted = userDetailFile.delete();

        }
        FrontEngine.getInstance().userData = null;
        return deleted;
    }
*/

    //    Chating
//    public void updataFirebaseToken(String userId) {
//        Map<String, Object> childUpdates = new HashMap<>();
//
//        childUpdates.put("/" + FireBaseDb.getInstance().tableUsers
//                        + "/" + userId
//                        + "/" + KEY_USER_UDID
//                , FirebaseInstanceId.getInstance().getToken());
//        FireBaseDb.getInstance().getDbReference().updateChildren(childUpdates);
//    }
}
