package com.example.shayan.taskapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shayan.taskapp.MainActivity;
import com.example.shayan.taskapp.R;


public class ContactUsFragment extends BaseFragment implements View.OnClickListener {

    private View rootView;
    private RatingBar ratingBar;
    Button Submitbtn;
    private TextView txtRatingValue;


    public ContactUsFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.item_contact_us, container,
                false);

        ratingBar=new RatingBar(getActivity());
        initialWork();
        initializeControls();
        setOnClickListeners();
        return rootView;
    }


    public void initialWork() {
        ((MainActivity) getActivity()).initialWorkForAllFragments(getActivity(),
                getActivity().getString(R.string.conatct), View.VISIBLE, R.mipmap.icon_menu, View.GONE, 0);
    }
    private void initializeControls() {
        ratingBar = (RatingBar) rootView.findViewById(R.id.rating);
        Submitbtn = (Button) rootView.findViewById(R.id.submitbtn);
        Submitbtn.setOnClickListener(this);


    }

    public void setOnClickListeners() {
        ((MainActivity) getActivity()).llMenu1.setOnClickListener(this);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener(){
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {

                Toast.makeText(getActivity(),
                        "Rating changed, current rating "+ ratingBar.getRating(),
                        Toast.LENGTH_SHORT).show();
            }
        });;
        Submitbtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMenu1:

                ((MainActivity) getActivity()).openAndCloseDrawer(true);
                break;
            case R.id.llMenu2:

//                ((MainActivity) getActivity()).replaceFragment(new PostJobFragment(),getActivity());
                break;
            case R.id.submitbtn:
                Toast.makeText(getActivity(), "FeedBack Submitted!!",
                        Toast.LENGTH_LONG).show();
        }
    }





}
