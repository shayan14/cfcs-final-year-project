package com.example.shayan.taskapp.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shayan.taskapp.Adapter.FeedAdapter;
import com.example.shayan.taskapp.MainActivity;
import com.example.shayan.taskapp.R;
import com.example.shayan.taskapp.model.FeedModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class NewsFeedFragment extends BaseFragment implements View.OnClickListener, FeedAdapter.ItemClickListener {

    private View rootView;
    Context context;
    RecyclerView recyclerview;
    private ArrayList<FeedModel> arrListCategories;// = new ArrayList<>();
    FeedAdapter adapter;

    TextView txtNoFound;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_feed, container,
                false);

        initialWork();
        initializeControls();
        setOnClickListeners();
        setRecyclerView();
        return rootView;
    }


    public void initialWork() {
        ((MainActivity) getActivity()).initialWorkForAllFragments(getActivity(),
                getActivity().getString(R.string.news_feed), View.VISIBLE, R.mipmap.icon_menu, View.VISIBLE, R.mipmap.add_button);
    }

    private void initializeControls() {
        recyclerview = (RecyclerView) rootView.findViewById(R.id.recyclerview);
        //txtNoFound = (TextView) rootView.findViewById(R.id.txtNoFound);
    }

    public void setOnClickListeners() {
        ((MainActivity) getActivity()).llMenu1.setOnClickListener(this);
        ((MainActivity) getActivity()).llMenu2.setOnClickListener(this);
    }

    public void setRecyclerView() {
        setData();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(mLayoutManager);

        adapter = new FeedAdapter(getActivity(), arrListCategories);
        recyclerview.setAdapter(adapter);
        adapter.setClickListener(this);
    }


    public void setData() {
        arrListCategories = new ArrayList<>();
        FeedModel newmodel = new FeedModel();
        FeedModel newmodel1 = new FeedModel();
        FeedModel newmodel2 = new FeedModel();
        FeedModel newmodel3 = new FeedModel();

        ////
        newmodel.setId(1);
        newmodel.setLikes(250);
        newmodel.setComments(100);
        newmodel.setName("Robert Downey Jr.");
        newmodel.setPostpic(R.mipmap.img_post);
        newmodel.setPropic(R.mipmap.propic1);
        newmodel.setStatus("Hahaha Check this out guys");
        newmodel.setTime("12 hrs");
        newmodel1.setId(2);
        newmodel1.setLikes(50);
        newmodel1.setComments(10);
        newmodel1.setName("Jennifer Anniston");
        newmodel1.setPostpic(R.mipmap.img_post2);
        newmodel1.setPropic(R.mipmap.propic2);
        newmodel1.setStatus("MEME from the series.");
        newmodel1.setTime("10 hrs");
        newmodel2.setId(3);
        newmodel2.setLikes(1000);
        newmodel2.setComments(10000);
        newmodel2.setName("Goaty McGoatFace");
        newmodel2.setPostpic(R.mipmap.img_post3);
        newmodel2.setPropic(R.mipmap.goat);
        newmodel2.setStatus("#TrueStory");
        newmodel2.setTime("5 mins ago");
        newmodel3.setId(4);
        newmodel3.setLikes(50);
        newmodel3.setComments(1000);
        newmodel3.setName("Benedict Cumberbatch");
        newmodel3.setPostpic(R.mipmap.img_post_four);
        newmodel3.setPropic(R.mipmap.propic4);
        newmodel3.setStatus("Scenes from the Hobbit");
        newmodel3.setTime("1 mins ago");


        arrListCategories.add(newmodel);
        arrListCategories.add(newmodel1);
        arrListCategories.add(newmodel2);
        arrListCategories.add(newmodel3);


//         arrListCategories.add(categoryModel);
//        arrListCategories.add(categoryModel);
//        arrListCategories.add(categoryModel);
//        arrListCategories.add(categoryModel);
//        arrListCategories.add(categoryModel);
//        arrListCategories.add(categoryModel);

        // arrListCategories = FrontEngine.getInstance().getArrListCategory(getActivity());

       /* if(arrListCategories.size()>0){
            txtNoFound.setVisibility(View.GONE);
        }else {
            txtNoFound.setVisibility(View.VISIBLE);
        }*/
    }

    public void showDialog(int position) {
        final Dialog dialog = new Dialog(getContext(),android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.setContentView(R.layout.feed_post_dialog);
        dialog.setTitle("");

        // set the custom dialog components - text, image and button
        ImageView image = (ImageView) dialog.findViewById(R.id.post_img);
        ImageView cross=(ImageView) dialog.findViewById(R.id.cross_img) ;
        Picasso.get().load(arrListCategories.get(position).getPostpic()).into(image);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    @Override
    public void onClick(View v) {
    }

    @Override
    public void onItemClick(View view, int position) {
        String img = getTag();
        if (img == getTag()) {
            showDialog(position);
        }

    }
}

