package com.example.shayan.taskapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.example.shayan.taskapp.R;
import com.example.shayan.taskapp.model.UserModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder> {

    private ArrayList<UserModel> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener,mClickListener2;
    Context context;

    // data is passed into the constructor
    public FriendsAdapter(Context context, ArrayList<UserModel> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context=context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_home, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        UserModel item= getItem(position);

        String Name = item.getName();
        int Id=mData.get(position).getId();
        holder.myTextView.setText(Name+"" +Id );
        String ProPic = item.getPropic();
//        Glide.with(context).load(ProPic).into(holder.myImgView);
        Picasso.get().load(ProPic).into(holder.myImgView);

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        LinearLayout linearLayout;
        ImageView myImgView;

        ViewHolder(View itemView) {
            super(itemView);
            myImgView = itemView.findViewById(R.id.tvAnimalPic);
            myTextView = itemView.findViewById(R.id.tvAnimalName);
            linearLayout = itemView.findViewById(R.id.llitem);
            myImgView.setOnClickListener(this);
            linearLayout.setOnClickListener(this);
            myImgView.setTag("Image");
        }

        @Override
        public void onClick(View view) {
            if (view.getTag() == "Image") {
                mClickListener.onImageClick(myImgView, getAdapterPosition());
            }
            else
            {
                if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            }

            }
        }

    // convenience method for getting data at click position
    UserModel getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
        this.mClickListener2 = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
        void onImageClick(View view,int position);
    }



}


